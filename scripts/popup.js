// Below is what we'll log to the console.

let regex = /https:\/\/github.com\/(\w+)\/(\w+)\/pull\/(\d+)\.patch/

browser.tabs.query({
  active: true,
  currentWindow: true,
}).then(tabs => {
  let tabId = tabs[0].id;
  browser.messageDisplay.getDisplayedMessage(tabId).then((message) => {
    console.log(message);
    return message.id
  }).then((id) => {
    console.log(id);
    return browser.messages.getFull(id)
  }).then((message) => {
      console.log(message.parts[0].parts[0]);
      let body = message.parts[0].parts[0].body;
      let matches = body.match(regex);
      console.log(matches);
      return `https://api.github.com/repos/${matches[1]}/${matches[2]}/pulls/${matches[3]}`;
  }).then((url) => {
      console.log(url);
      return fetch(url, {
          headers: {
              'Content-Type': 'application/vnd.github.v3.patch',
          }
      });
  }).then((res) => {
      console.log(res);
      return res.text();
  }).then((text) => {
      document.body.textContent = text;
  });
});

